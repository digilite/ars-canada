<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header id="stickyMenu">
        <div class="header-top">
            <div class="container flex-content">
                <div class="header-left-part">
                    <a class="navbar-brand flex-content" href="<?php echo site_url(); ?>">
                        <img class="ars-logo" src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="ARS_Canada" />
                        <img class="logo-part" src="<?php bloginfo('template_url'); ?>/img/ars-canada-logo-text.png" alt="ARS-Canada" />
                    </a>
                </div>
                <div class="header-right-part hidden-xs">
                    <ul class="social-icons">
                        <?php get_template_part( 'social'); ?>
                    </ul>
                    <a href="<?php echo get_page_link(2351); ?>" class="btn white-btn short-btn btn-join-us">become a member</a>
                    <a href="<?php echo get_page_link(687); ?>" class="btn black-btn short-btn">donate</a>
                </div>
            </div>
        </div>
        <nav class="navbar" id="mNavbar">
            <div class="container">
                <div class="navbar-header">
                    <div class="menu-back">
                        <a href="javascript:void(0)" class="closebtn">
                            <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div id="nav-icon" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span></span>
<!--                        <span></span>-->
                        <span></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php if ( has_nav_menu( 'primary-menu' )) : ?>
                        <?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container'=> '','menu_class'=>'nav navbar-nav')); ?>
                    <?php endif; ?>
                    <div class="header-right-part visible-xs">
                        <ul class="social-icons">
                            <?php get_template_part( 'social'); ?>
                        </ul>
                        <a href="<?php echo get_page_link(2351); ?>" class="btn white-btn short-btn btn-join-us">join us</a>
                        <a href="<?php echo get_page_link(687); ?>" class="btn black-btn short-btn">donate</a>
                    </div>
                </div>
            </div>
        </nav>
        <div class="form-popup" id="myForm">
            <div class="register-form">
                <button type="button" class="cancel" onclick="closeForm()">X</button>
                <?php echo do_shortcode('[formidable id=8]'); ?>
            </div>
        </div>
	</header>
<main>