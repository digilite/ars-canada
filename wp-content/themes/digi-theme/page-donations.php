<?php get_header(); ?>
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="container-fluid donations">
                <div class="donations-page flex-content">
                    <div class="donation-form">
                        <!--                    <h1 class="section-title maitree-font">--><?//= get_the_title(); ?><!--</h1>-->
                        <iframe src="https://www.canadahelps.org/en/dn/39733" scrolling="no" seamless="seamless" frameborder="0"></iframe>
                    </div>
                    <div class="donation-page-content">
                        <div class="donation-content">
                            <p class="circle"><strong>i</strong></p>
                            <?= get_the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer(); ?>