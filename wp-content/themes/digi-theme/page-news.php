<?php /* Template Name: News */
get_header();
if ( get_query_var('paged') ) {
    $paged = get_query_var('paged');
} elseif ( get_query_var('page') ) { // 'page' is used instead of 'paged' on Static Front Page
    $paged = get_query_var('page');
} else {
    $paged = 1;
}
$posts_per_page = 9;
$custom_query_args = array(
    'post_type' => 'cpt_news',
    'posts_per_page' => $posts_per_page,
    'paged' => $paged,
    'post_status' => 'publish',
    'ignore_sticky_posts' => true,
    'order' => 'DESC', // 'ASC'
    'orderby' => 'date'
);
$custom_query = new WP_Query( $custom_query_args );

$count_posts = wp_count_posts( 'cpt_news' );
$published_posts = $count_posts -> publish;
$page_count = ceil( $published_posts / $posts_per_page );

if ( $custom_query->have_posts() ) : ?>
<div class="page-content">
    <div class="container">
        <?php custom_breadcrumbs(); ?>
        <h1 class="section-title maitree-font">News</h1>
        <div class="row">
            <?php $i = 0; while( $custom_query->have_posts() ) : $i++; $custom_query->the_post(); ?>

                <div class="col-sm-4">
                    <div class="events-item">
                        <a target="_blank" href="<?= get_field('event_link'); ?>" class="absolute"></a>
                        <div class="events-img" style="background:url('<?= get_the_post_thumbnail_url(); ?>')  no-repeat top / cover;"></div>
                        <span class="date"><?= get_field('event_date'); ?></span>
                        <h2 class="events-title"><?= get_the_title(); ?></h2>
                    </div>
                </div>
                <?php if($i%3===0): ?>
                    <div class="clearfix"></div>
                <?php endif; ?>

            <?php endwhile; ?>
        </div>

        <?php if ($custom_query->max_num_pages > 1) :
            $orig_query = $wp_query;
            $wp_query = $custom_query;
            ?>
            <nav class="prev-next-posts">
                <div class="next-posts-link circle">
                    <?php
                        if(get_previous_posts_link() == ! null){
                            echo get_previous_posts_link( '<i class="fa fa-long-arrow-left"></i>' );
                        }else{
                            echo '<i class="fa fa-long-arrow-left"></i>';
                        }
                    ?>
                </div>
                <p class="page-count"> Page: <?= $paged; ?> / <?= $page_count; ?> </p>
                <div class="prev-posts-link circle">
                    <?php
                        if(get_next_posts_link() == ! null){
                            echo get_next_posts_link( '<i class="fa fa-long-arrow-right"></i>', $custom_query->max_num_pages );
                        }else{
                            echo '<i class="fa fa-long-arrow-right"></i>';
                        }
                    ?>
                </div>
            </nav>
            <?php $wp_query = $orig_query; ?>
        <?php endif; ?>

        <?php
            wp_reset_postdata(); // reset the query
            endif;
        ?>
    </div>
</div>

<?php get_footer(); ?>

