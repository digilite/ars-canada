<?php /* Template Name: Executive Board */
get_header(); ?>
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="page-content">
            <div class="container">
                <?php custom_breadcrumbs(); ?>
                <h1 class="section-title maitree-font"><?php the_title(); ?></h1>
                <?php if(!empty(get_field('team_members'))): ?>
                    <div class="row">
                        <?php
                            $team_members = get_field('team_members');
                            foreach($team_members as $team_member):
                                $name_surname = $team_member['name_surname'];
                                $position = $team_member['position'];
                                $member_image = $team_member['member_image'];
                        ?>
                                <div class="col-md-3 col-sm-4">
                                    <div class="member-box">
                                        <div class="member-img" style="background:url('<?= $member_image; ?>')  no-repeat top / cover;"></div>
                                        <h4 class="name-surname"><?= $name_surname; ?></h4>
                                        <p class="position"><?= $position; ?></p>
                                    </div>
                                </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <?php if(!empty(get_the_content())): ?>
                    <div class="row">
                       <div class="content">
                           <p class="circle"><strong>i</strong></p>
                            <?php the_content(); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>

