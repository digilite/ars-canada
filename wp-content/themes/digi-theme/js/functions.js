var $ = jQuery;
var map = document.getElementById("map");
var isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
var header = document.getElementById("stickyMenu");
var sticky = header.offsetTop;

var events_slider = {
	center: true,
	items:2,
	loop:true,
	margin:30,
	nav:true,
	navText: ["<i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i>","<i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i>"],
	responsive:{
		0:{
			items:1
		},
		600:{
			items:2
		},
		1000:{
			items:4
		}
	}
};
var banner_slider = {
	items:1,
	autoplay: true,
	animateIn: "fadeIn",
	animateOut: "fadeOut",
	autoplayTimeout: 5000,
	responsiveClass: true,
	dots: true,
	nav: false
}

var accomplishment_slider = {
	items:1,
	autoplay: true,
	infinite: true,
	loop: true,
	slideTransition: "linear",
	autoplayTimeout: 250,
	autoplaySpeed: 5000
}

var eventsSlider = $(".events-slider");
var bannerSlider = $(".banner-slider");
var accomplishmentSlider = $(".accomplishment-slider");
var showBtn = $(".btn.show-content");
var activeLink = $(".chapters_settings h4");
var chaptersEmail = $(".chapters-email").text();

function openForm() {
	$("#field_hidden_email").val(chaptersEmail);
	document.getElementById("myForm").style.display = "block";
}
function closeForm() {
	document.getElementById("myForm").style.display = "none";
}

function stickyMenu() {
	if (window.pageYOffset > sticky) {
		header.classList.add("sticky");
	} else {
		header.classList.remove("sticky");
	}
}

$(document).ready(function() {
	if(map !== null && map !== ""){
		initMap();
	}

	$("#nav-icon").on("click", function(){
		if($(".navbar-collapse").hasClass("in")==false){
			$("body").addClass("fixedPosition");
		}else{
			$("body").removeClass("fixedPosition");
		}
	});

	if(isMobile){
		$(".menu-item-has-children").on("click", function(){
			$(this).children(".sub-menu").css({"display": "block", "width": "100%"});
			$(".menu-back").css({"display": "block"});
		});

		$(".closebtn").on("click", function(){
			$(".sub-menu").css({"width": "0", "display": "none"});
			$(".menu-back").css({"display": "none"});
		})
	}

	$(".change-view").click(function(e){
		var ajaxurl = window.location.protocol + "//" + window.location.host +"/ars-canada/wp-admin/admin-ajax.php";
		var valueSelect = $(this).attr("data-view");
		var catValue = $(this).attr("data-cat");

		if(valueSelect === "calendar-view"){
			$(this).text("List View");
			$(this).attr("data-view", "list-view");
		}else{
			$(this).text("Calendar View");
			$(this).attr("data-view", "calendar-view");
		}

		$.ajax( ajaxurl, {
			type: "POST",
			data: { action: "change_event_view",
				view_value:	valueSelect,
				cat_value: catValue
			},
			dataType: "json",
			complete : function ( response ) {   // to develop always
			},
			success: function ( response ) {     // to develop in case of correct AJAX call
				if ( response.success ) {
					calendar = response.html;
					$("#calendar").html( calendar );

				} else {
					$("#calendar").html( "<span>No events available</span>" );
				}
			},
			error: function ( errorThrown ) {   // to develop in case of AJAX call error
				console.log( errorThrown );
			},
		});
	})

	$(".chapters_settings h4:first-child").addClass("active");

	$(activeLink).click(function(e){
		if($(".chapters_settings h4").hasClass("active")){
			$(".chapters_settings h4").removeClass("active");
			$(this).addClass("active");
		}
	});

	$(".smoothscroll, .smoothscroll a").click(function(e) {
		e.preventDefault();
		var t=$(this).attr("href");
		$("html,body").animate( {
				scrollTop: $(t).offset().top-300
			}
			, "slow")
	});
    $(".user-content iframe").each(function(){
        $(this).wrap("<div class='embed-responsive embed-responsive-16by9'></div>")
    });
	$(showBtn).click(function(e){
		e.preventDefault();
		$(".hide-impacts").css({"display": "block", "visibility": "visible", "opacity": 1});
	});

	if(eventsSlider.length) {
		eventsSlider.owlCarousel(events_slider);
	}
	if(bannerSlider.length) {
		bannerSlider.owlCarousel(banner_slider);
	}
	if(accomplishmentSlider.length) {
		accomplishmentSlider.owlCarousel(accomplishment_slider);
	}
});

$(window).load(function() {
	var viewHeight = $(window).height();
	var bodyHeight = $("body").outerHeight();

	if(bodyHeight <= viewHeight) {
		$("body").css("min-height", viewHeight);
		$("footer").addClass("absolute-bottom");
	}
});

window.onscroll = function() {
	stickyMenu();

	$(".donate-section").addClass("scrolled");
	if ((window.innerHeight + Math.ceil(window.pageYOffset)) >= document.body.offsetHeight) {
		$(".donate-section").removeClass("scrolled");
	}


};

// UNCOMMENT FOLLOWING CODE IF YOU WANT TO PREVENT RIGHT CLICK OR INSPECT ELEMENT
// $(document).on("contextmenu", function() {
// 	return false; //Prevent right click
// });

// $(document).keydown(function(event) {
// 	if(event.keyCode == 123) {
// 		return false; //Prevent from f12
// 	} else if(event.ctrlKey && event.shiftKey && event.keyCode == 73) {
// 		return false; //Prevent from ctrl+shift+i
// 	}
// });