<?php
/* Template Name: Contacts Page */
get_header(); ?>
    <div class="main-content">
        <div class="container">
            <?php custom_breadcrumbs(); ?>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="contact-form">
                        <h1 class="maitree-font form-title">Contact Us</h1>
                        <?php echo do_shortcode('[formidable id=6]'); ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-1 col-sm-5">
                    <div id="map" class="maps"></div>
                </div>
                <div class="col-md-2 col-md-offset-1 col-sm-3">
                    <?php
                        $email = get_field('email', 'option');
                        $address = get_field('address', 'option');
                        $phone = get_field('phone', 'option');
                    ?>
                    <div class="contact-info text-right">
                        <?php if(!empty($address)): ?>
                        <div class="info">
                            <h4 class="contact-t">Address</h4>
                            <p class="contact-i"><?=$address; ?></p>
                        </div>
                        <?php endif; ?>
                        <?php if(!empty($phone)): ?>
                        <div class="info">
                            <h4 class="contact-t">Phone </h4>
                            <p class="contact-i"><a href="tel:<?= $phone; ?>"><?= $phone; ?></a></p>
                        </div>
                        <?php endif; ?>
                        <?php if(!empty($email)): ?>
                            <div class="info">
                                <h4 class="contact-t">E-mail</h4>
                                <p class="contact-i"><a href="mailto:<?= $email; ?>"><?= $email; ?></a></p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>