<?php get_header(); ?>
<?php
    $programs = get_field('programs');
?>
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="page-content">
                <div class="container">
                    <?php custom_breadcrumbs(); ?>
                    <h1 class="section-title maitree-font"><?= get_the_title(); ?></h1>
                    <?php if(!empty($programs)): ?>
                        <div class="row">
                            <?php $i=0; foreach($programs as $program): $i++; ?>
                                <div class="col-sm-6">
                                    <div class="program-box">
                                        <a href="<?= $program['programe_page_link']; ?>" class="absolute"></a>
                                        <img src="<?= $program['programe_icon']; ?>" alt="" />
                                        <div>
                                            <h2 class="program-title"><?= $program['programe_name']; ?></h2>
                                            <?php $program_cities = $program['program_cities'];
                                            if(!empty($program_cities)):
                                            foreach($program_cities as $program_city): ?>
                                                <span class="program-city"><?= $program_city['city_name']; ?></span>
                                            <?php endforeach; endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if($i%2==0): ?>
                                    <div class="clearfix"></div>
                                <?php endif; ?>
                            <?php endforeach;?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer(); ?>