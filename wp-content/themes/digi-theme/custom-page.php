<?php
/* Template Name: Custom Page */
get_header(); ?>
<?php
    $bg = get_the_post_thumbnail_url(get_the_ID(), 'full');
    $one_column_block = get_field('one_column_block');
    $one_col_text = $one_column_block['block_description'];
    $one_col_title = $one_column_block['block_name'];
    $two_column_block = get_field('two_column_block');
    $two_col_text = $two_column_block['block_text'];
    $two_col_title = $two_column_block['block_title'];
?>
    <div class="container">
        <?php custom_breadcrumbs(); ?>
        <h1 class="section-title maitree-font"><?= get_the_title(); ?></h1>
    </div>
    <div class="main-conten custom-page">
        <div class="featured-banner" style="background:url('<?= $bg; ?>')  no-repeat top / cover;"></div>
        <div class="container">
            <?php if(!empty($one_col_text) || !empty($one_col_title)): ?>
                <div class="one-column-block">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="one-column-text">
                                <?= $one_col_text; ?>
                            </div>
                            <h1 class="one-column-title">
                                <?= $one_col_title; ?>
                            </h1>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(!empty($two_column_block)): ?>
                <div class="two-column-block">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="flex-content">
                                <?php foreach($two_column_block as $block): ?>
                                    <div class="col">
                                        <h2 class="two-column-title">
                                            <?= $block['block_title']; ?>
                                        </h2>
                                        <div class="two-column-text">
                                            <?= $block['block_text']; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php get_footer(); ?>