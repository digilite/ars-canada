<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
    global $post;
    $post_slug = $post->post_name;

    $subtitle = get_field('chapter_subtitle');
    $activities_functions = get_field('activities_functions');
    $events_calendar = get_field('calendar_of_events');
    $term_slug = $events_calendar->slug;
    $organizations = get_field('organizations');
    $organization_text = get_field('organization_text');
    $chapters_programs = get_field('chapters_programs');
    $chapters_publication = get_field('related_publications');
    $chapters_contact_details = get_field('chapters_contact_details');
?>
    <div class="container">
        <?php custom_breadcrumbs(); ?>
        <h1 class="chapters-title maitree-font"><?php the_title(); ?></h1>
        <h5 class="chapters-subtitle"><?= $subtitle; ?></h5>
        <div class="row">
            <div class="col-sm-4 hidden-xs">
                <div class="chapters_settings">
                    <?php if(!empty($events_calendar)): ?>
                        <h4><a href="#events" class="smoothscroll">Calendar Of Events</a></h4>
                    <?php endif; ?>
                    <?php if(!empty($activities_functions)): ?>
                        <h4><a href="#activities" class="smoothscroll">News</a></h4>
                    <?php endif; ?>
                    <?php if(!empty($events_calendar)): ?>
                        <h4><a href="#organization" class="smoothscroll">Organization</a></h4>
                    <?php endif; ?>
                    <?php if(!empty($chapters_programs)): ?>
                        <h4><a href="#programs" class="smoothscroll">Programs</a></h4>
                    <?php endif; ?>
                    <?php if(!empty($chapters_publication)): ?>
                        <h4><a href="#publications" class="smoothscroll">Publications</a></h4>
                    <?php endif; ?>
                    <?php if(!empty($chapters_contact_details)): ?>
                        <h4><a href="#contacts" class="smoothscroll">Contacts</a></h4>
                    <?php endif; ?>
                </div>

            </div>
            <div class="col-sm-8">
                <div class="chapters-settings">
                    
                    <?php if(!empty($events_calendar)): ?>
                    <div class="calendar-of-events available-features" id="events">
                        <div class="flex-content">
                            <h4 class="chapters-content-title">Calendar Of Events</h4>
                            <button data-cat="<?= $term_slug; ?>" data-view="calendar-view" class="btn-gray change-view">List view</button>
                        </div>
                        <div id="calendar">
                            <?php echo do_shortcode('[tribe_events cat="'.$term_slug.'" limit="2"]');?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php 
                        $args = array(
                            "post_type" => "post",
                            "category_name" => $post_slug,
                        );
                        $news = new WP_Query( $args );
                        
                        $post_thumbnail_id = get_post_thumbnail_id( $post );
                        $image_url = wp_get_attachment_image_url( $post_thumbnail_id, "full");
                        $terms = get_the_terms($post,"category");
                        if ( $news->have_posts()): ?>
                        <div class="activities-functions available-features" id="activities">
                            <h4 class="chapters-content-title">News</h4>
                            <div class="flex-content">
                                <?php
                                    $once = false;
                                    $check_archive = false;
                                    while ($news->have_posts()) : $news->the_post(); 
                                    if (has_category("archives")):
                                        if ( !$check_archive && !$once ):
                                            $check_archive = true;
                                            $once = true;
                                        endif;
                                    else:
                                ?>
                                    <div class="news-box">
                                        <a href="<?php the_permalink($post_id); ?>" class="absolute"></a>
                                        <div class="news-img" style="background:url('<?= $image_url; ?>')  no-repeat top / cover;"></div>
                                        <span class="date">
                                             <?php //$date = date_format(date_create($activity->post_date),"d M, Y");
                                                    //echo $date; ?>
                                        </span>
                                        <h2 class="news-title"><?php the_title(); ?></h2>
                                    </div>
                                <?php endif; endwhile; ?>
                            </div>
                            <?php if($check_archive): ?>
                                <div class="text-right margin-top-40">
                                    <a href="<?php echo site_url(); ?>/category/archives+<?= $post_slug?>" class="btn black-btn long-btn">View News/Activities Archive</a>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; wp_reset_postdata(); ?>

                    <?php if(!empty($organizations)): ?>
                        <div class="organizations available-features" id="organization">
                            <h4 class="chapters-content-title">Organization</h4>
                            <div class="organization_text maitree-font">
                                <?= $organization_text; ?>
                            </div>
                            <div class="organization-items flex-content">
                                <?php foreach($organizations as $organization): ?>
                                    <div class="organization-item">
                                        <a href="<?= $organization['page_link']; ?>" class="absolute"></a>
                                        <img src="<?= $organization['icon']; ?>" alt="" />
                                        <h5><?= $organization['title']; ?></h5>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(!empty($chapters_programs)): ?>
                        <div class="chapters-programs available-features" id="programs">
                            <h4 class="chapters-content-title">Programs</h4>
                            <div class="flex-content">
                                <?php foreach($chapters_programs as $chapters_program): ?>
                                    <div class="chapter-program flex-content">
                                        <a href="<?= $chapters_program['program_page_link']; ?>" class="absolute"></a>
                                        <img src="<?=  $chapters_program['program_icon']; ?>" alt="" />
                                        <h5><?= $chapters_program['program_title']; ?></h5>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(!empty($chapters_publication)): ?>
                        <div class="chapters-publication available-features" id="publications">
                            <h4 class="chapters-content-title">Publications</h4>
                            <?php foreach($chapters_publication as $publication): ?>
                                <?php $publication_file = get_field('publication_file1', $publication->ID); ?>
                                <div class="publication-item flex-content">
                                    <p class="publication-title">
                                    <span class="publication-year">
                                        <?php the_field('publication_year', $publication->ID); ?>
                                    </span>
                                        <a href="<?= $publication_file; ?>"><?= $publication->post_title; ?></a>
                                    </p>
                                    <p class="publication-icon"><a href="<?= $publication_file; ?>" download>
                                            <img class="pdf-icon" src="<?php bloginfo('template_url'); ?>/img/pdf.svg" alt="pdf-file" />
                                        </a></p>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="text-right margin-top-40">
                            <a href="<?php echo get_permalink($publication->ID); ?>" class="btn black-btn long-btn">View Publications</a>
                        </div>
                    <?php endif; ?>

                    <?php 
                        if( have_rows("chapters_contact_details") ): while ( have_rows("chapters_contact_details") ) : the_row();
                            $email = get_sub_field("chapters_email");
                    ?>
                        <div class="chapters-contact available-features" id="contacts">
                            <h4 class="chapters-content-title">Contacts</h4>
                            <div class="flex-content">
                                <div class="hr-line"></div>
                                <div class="chapters-contact-details">
                                    <div class="chapter-contact-info">
                                        <p><?php the_sub_field("chapters_address"); ?></p>
                                        <p><?php the_sub_field("chapters_phone"); ?></p>
                                        <p class="chapters-email"><?= $email ; ?></p>
                                    </div>
                                    <?php if(!empty($email)): ?>
                                        <a href="mailto:<?= $email; ?>" class="btn black-btn full-width-btn">Send a message</a>
                                    <?php endif; ?>
                                </div>
                                <div class="chapters-contact-details flex-content">
                                    <div class="contact-socials text-right">
                                        <?php if( have_rows("social_medias") ): while ( have_rows("social_medias") ) : the_row(); ?>
                                            <a class="social-icon" href="<?php the_sub_field("url"); ?>" title="<?php the_sub_field("social_title"); ?>"  target="_blank"><?php the_sub_field("icon"); ?></a>
                                        <?php endwhile; endif; ?>
                                    </div>
                                    <a href="<?php echo get_page_link(2351); ?>" class="btn black-btn full-width-btn margin-auto open-button">Join us</a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
