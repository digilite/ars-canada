    </main>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <h1 class="subscribe-title maitree-font">Subscribe to stay updated</h1>
                        <div class="subscribe-form">
                            <?php echo do_shortcode('[formidable id=7]'); ?>
                        </div>
                    </div>
                    <div class="col-md-5 col-md-offset-3 col-sm-6 text-right">
                        <div class="footer-right-top">
                            <ul class="social-icons">
                                <?php get_template_part( 'social'); ?>
                            </ul>
                            <a href="<?php echo site_url(); ?>/donations" class="btn white-btn medium-btn">Donate</a>
                            <div class="copyright">
                                <p>Armenian Relief Society<br>With the People, For the People</p>
                                <p>Հայ Օգնութեան Միութիւն<br>Ժողովուրդիս հետ, ժողովուրդիս համար</p>
                                <p>&copy; <?php echo date("Y"); ?> ARS Canada. All rights reserved<br>
                                    ARS Inc. is an NGO on the Roster in Consultative Status<br> with UN ECOSOC and in Association with the DPI
                                </p>
                                <p>Made by <a href="//digilite.ca/" target="_blank">Digilite</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>