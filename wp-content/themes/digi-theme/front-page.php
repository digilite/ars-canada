<?php 
    get_header();
    get_template_part( 'templates/banner-section');
    get_template_part( 'templates/accomplishments-section');
    get_template_part( 'templates/our-chapters-section');
    get_template_part( 'templates/events-section');
    get_template_part( 'templates/latest-news-section');
    get_template_part( 'templates/our-impact-section');
?>
    <section class="become-member-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-1 hidden-sm">
                    <div class="member-icon">
                        <img src="<?php bloginfo('template_url'); ?>/img/become_member.svg" alt="become-member" />
                    </div>
                </div>
                <div class="col-sm-7">
                    <h4><strong>Take action. Become a member.</strong></h4>
                    <p>Volunteer your time and skills and make a difference in the lives of others.</p>
                </div>
                <div class="col-md-2 col-md-offset-2 col-sm-5 text-right">
                    <a href="<?php echo get_page_link(2351); ?>" class="btn black-btn short-btn">Apply now</a>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>