<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="page-content">
            <div class="container">
                <?php custom_breadcrumbs(); ?>
                <div class="row user-content">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h1 class="page-title maitree-font"><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>