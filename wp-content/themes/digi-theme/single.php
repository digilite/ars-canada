<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="page-content">
            <div class="container">
                <?php custom_breadcrumbs(); ?>
                <div class="row user-content">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h1 class="page-title maitree-font"><?php the_title(); ?></h1>
                        <div class="post-date"><?= get_the_date(); ?></div>
                        <?php the_content(); ?>
                            <a class="circle" href="javascript:void(0)" onclick="history.back(-1)">
                                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                            </a>&nbsp;&nbsp;&nbsp;&nbsp;back
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>