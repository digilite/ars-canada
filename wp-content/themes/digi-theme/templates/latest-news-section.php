<section class="section latest-news-section">
    <?php
    $loop = new WP_Query(
        array(
            'post_type' => 'post',
            'posts_per_page' => 9,
            'category__not_in' => 80,
            'order' => 'DESC',
            )
        );
        if ( $loop->have_posts() ) : ?>
            <div class="container">
                <h1 class="section-title maitree-font">Latest news</h1>
                <div class="row">
                    <?php $i = 0; while ( $loop->have_posts() ) : $i++; $loop->the_post(); ?>
                    <div class="col-sm-4">
                        <div class="news-box">
                            <?php $news_link = get_field('news_link', get_the_ID());
                                if(!empty($news_link)): ?>
                                    <a href="<?= $news_link; ?>" target=_blank; class="absolute"></a>
                                    <?php else: ?>
                                    <a href="<?php the_permalink(); ?>" class="absolute"></a>
                                    <?php endif; ?>
                                    <?php
                                if(!empty(get_the_post_thumbnail_url())){
                                    $bg = get_the_post_thumbnail_url();
                                }else{
                                    $bg = get_bloginfo('template_url')."/img/default.jpg";
                                }
                                ?>
                            <div class="news-img" style="background:url('<?= $bg; ?>')  no-repeat top / cover;"></div>
                            <span class="date"><?php the_date(); ?></span>
                            <h2 class="news-title"><?php the_title(); ?></h2>
                        </div>
                    </div>
                    <?php if($i%3===0): ?>
                    <div class="clearfix"></div>
                    <?php endif; ?>
                    <?php endwhile; ?>
                </div>
                <div class="read-more text-right">
                    <a href="<?php echo site_url(); ?>/category/news-activities" class="btn black-btn long-btn">view all</a>
                </div>
            </div>
            <?php endif; ?>
    <?php wp_reset_postdata(); ?>
</section>