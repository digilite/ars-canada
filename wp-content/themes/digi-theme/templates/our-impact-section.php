<section class="section our-impact-section hidden-xs">
    <?php if(get_field('our_impacts')): ?>
        <div class="container">
            <h1 class="section-title maitree-font">Our Impact</h1>
            <div class="row">
                <?php
                $impacts = get_field('our_impacts');
                $i = 0;
                foreach ($impacts as $impact): $i++;
                $impact_amount = $impact['impact_amount'];
                $impact_text = $impact['impact_text'];
                if($i > 6){
                    $class = 'hide-impacts';
                }else{
                    $class = '';
                }
            ?>
                <div class="col-md-4 col-sm-6 <?= $class; ?>">
                        <div class="impact-data"><?= $impact_amount; ?></div>
                        <div class="impact-desc"><?= $impact_text; ?></div>
                    </div>
                <?php if($i%2==0): ?>
                <div class="clearfix visible-sm"></div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</section>