<section class="events-section">
    <?php
    $loop = new WP_Query(
        array(
            'post_type' => 'tribe_events',
            'posts_per_page' => -1
        )
    );
    if ( $loop->have_posts() ) : ?>
        <div class="container">
            <h1 class="section-title maitree-font">Events</h1>
        </div>
        <div class="events-slider">
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="events-item">
                    <a target="_blank" href="<?= get_the_permalink(); ?>" class="absolute"></a>
                    <div class="events-img" style="background:url('<?= get_the_post_thumbnail_url(); ?>')  no-repeat top / cover;"></div>
                    <span class="date"><?= get_the_date(); ?></span>
                    <h2 class="events-title"><?= get_the_title(); ?></h2>
                </div>
            <?php endwhile; ?>
        </div>
        <?php endif;
    wp_reset_postdata();
    ?>
</section>


