<section class="banner-section">
    <?php if( have_rows("slider") ): ?>
        <div class="banner-slider">
            <?php while ( have_rows("slider") ) : the_row();
                $title = get_sub_field("slider_title");
                $img = get_sub_field("slider_image"); ?>
                <div class="banner flex-content" style="background:url('<?= $img; ?>')  no-repeat top / cover;">
                    <div class="container">
                        <div class="banner-content">
                            <h1 class="maitree-font"><?= $title; ?></h1>
                            <?php if ($link = get_sub_field("slider_link")) : ?>
                                <a href="<?= $link; ?>" class="btn white-btn medium-btn">explore</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
</section>