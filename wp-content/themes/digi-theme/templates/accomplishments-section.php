<section class="section accomplishments-section">
    <div class="our-accomplishments flex-content">
        <div class="container m-top">
            <div class="flex-content">
                <div class="l-part">
                    <div class="accomplishments-text">Our Accomplishments
                        <i class="fa fa-caret-right"></i></div>
                </div>
                <div class="r-part">
                    <?php if(get_field('lines')): ?>
                        <div class="accomplishment-slider">
                            <?php $lines = get_field('lines');
                            foreach ($lines as $line): ?>
                                <p><?= $line['running_text']; ?></p>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

