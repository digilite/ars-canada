<section class="section our-chapters-section">
    <?php
    $loop = new WP_Query(
        array(
            'post_type' => 'cpt_chapter',
            'posts_per_page' => -1
            )
        );
        if ( $loop->have_posts() ) : ?>
        <div class="container">
            <h1 class="section-title maitree-font">Our chapters</h1>
            <div class="flex-content">
                <?php while ( $loop->have_posts() ) : $loop->the_post();
                    $subtitle = get_field('chapter_subtitle');
                    ?>
                    <div class="chapter-box">
                        <a href="<?= get_the_permalink(); ?>" class="absolute"></a>
                        <div class="chapter-img" style="background:url('<?= get_the_post_thumbnail_url(); ?>')  no-repeat top / cover;"></div>
                        <h4 class="chapter-title"><?= get_the_title(); ?></h4>
                        <h5 class="chapter-subtitle"><?= $subtitle; ?></h5>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
        <?php endif;
    wp_reset_postdata();
    ?>
</section>