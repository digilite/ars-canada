<?php get_header();
$wp_query->query;
$category = get_queried_object();
?>
    <div class="container page-content">
        <?php custom_breadcrumbs(); ?>
        <h1 class="section-title maitree-font"><?= $category->name; ?></h1>
        <?php if($category->slug === 'publications'): ?>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="chapters-publication">
                        <?php  while ( have_posts() ) : the_post(); ?>
                             <?php $publication_file = get_field('publication_file1', get_the_ID()); ?>
                            <div class="publication-item flex-content">
                                <p class="publication-title">
                                    <span class="publication-year">
                                        <?php the_field('publication_year', $publication->ID); ?>
                                    </span>
                                    <a href="<?= $publication_file; ?>"><?= get_the_title(); ?></a>
                                </p>
                                <p class="publication-icon">
                                    <a href="<?= $publication_file; ?>" download>
                                        <img class="pdf-icon" src="<?php bloginfo('template_url'); ?>/img/pdf.svg" alt="pdf-file" />
                                    </a>
                                </p>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>

        <?php else: ?>
            <div class="row">
                <?php $i = 0; while ( have_posts() ) : $i++; the_post(); ?>
                    <div class="col-sm-4">
                        <div class="news-box">
                            <?php $news_link = get_field('news_link', get_the_ID());
                            if(!empty($news_link)): ?>
                                <a href="<?= $news_link; ?>" target=_blank; class="absolute"></a>
                            <?php else: ?>
                                <a href="<?php the_permalink(); ?>" class="absolute"></a>
                            <?php endif; ?>
                            <?php
                            if(!empty(get_the_post_thumbnail_url())){
                                $bg = get_the_post_thumbnail_url();
                            }else{
                                $bg = get_bloginfo('template_url')."/img/default.jpg";
                            }
                            ?>
                            <div class="news-img" style="background:url('<?= $bg; ?>')  no-repeat top / cover;"></div>
                            <span class="date"><?php the_date(); ?></span>
                            <h2 class="news-title"><?php the_title(); ?></h2>
                        </div>
                    </div>
                    <?php if($i%3===0): ?>
                        <div class="clearfix"></div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
<?php get_footer(); ?>